<!DOCTYPE html>
<html>

<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<title>My First Calculator</title>
<style>

#num1,#num2{
	color:blue;
	text-align: center;
}

#addition,#subtraction,#multiplication,#division,#result{
	text-align: center;
}	
</style>
</head>


<body>
<form action="<?php echo htmlentities($_SERVER['PHP_SELF']); ?>" method="GET">
<div style="text-align:center">
<p>
<label for="num1">Num1</label>
<input type="text" name="num1" id="num1"/>

<label for="num2">Num2</label>
<input type="text" name="num2" id="num2"/>
</p>

<p>
<input type="radio" name="operation" value="Addition" id="addition"/>
<label for="addition">Addition</label>
</p>

<p>
<input type="radio" name="operation" value="Subtraction" id="subtraction"/>
<label for="subtraction">Subtraction</label>
</p>

<p>
<input type="radio" name="operation" value="Multiplication" id="multiplication"/>
<label for="multiplication">Multiplication</label>
</p>

<p>
<input type="radio" name="operation" value="Division" id="division"/>
<label for="division">Division</label>
</p>

<p>
<input type="submit" name="submit" value="Send"/>
</p>
</div>

</form>

<?php
if(isset($_GET['submit'])&&isset($_GET['operation'])&& is_numeric($_GET['num1'])&&is_numeric($_GET['num2'])){

	$value1 = $_GET['num1'];
	$value2 = $_GET['num2'];
	$action = $_GET['operation'];
	
	if($action == "Addition"){
		echo sprintf("<p id = 'result'>The Result is: %f + %f = %f</p>",$value1, $value2, $value1+$value2);
	}
	
	else if($action == "Subtraction"){
		echo sprintf("<p id = 'result'>The Result is: %f - %f = %f</p>", $value1, $value2, $value1-$value2);
	}
	
	else if($action == "Multiplication"){	
		echo sprintf("<p id = 'result'>The Result is: %f * %f = %f</p>",$value1, $value2, $value1*$value2);
	}
	
	else if($action == "Division" && $value2 == 0){
		echo sprintf("<p id = 'result'>Syntax Error: the divisor cannot be zero!");
	}else{
		echo sprintf("<p id = 'result'>The Result is: %f / %f = %f</p>",$value1, $value2, $value1/$value2);
	}
}else{
	echo sprintf("<p id = 'result'>Please type in two numbers and select an operation");
}
	
?>

</body>
</html>
